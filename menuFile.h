// File:        menuFile.h
// Author:      Paul McAvoy <paulmcav@queda.net>
// Org:
// Desc:        
//              
// 
// $Revision$

/*
 * $Log$
 * Revision 1.1  2001/08/09 08:04:44  paulmcav
 * Initial revision
 *
 * 
*/

#ifndef _MENUFILE_H_
#define _MENUFILE_H_

/*
*/

int open_new_file( const char *file_name );

#endif


