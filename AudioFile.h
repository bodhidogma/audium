// File:        audioFile.h
// Author:      Paul McAvoy <paulmcav@queda.net>
// Org:
// Desc:        
//              
// 
// $Revision$

/*
 * $Log$
 * Revision 1.8  2001/08/30 18:10:49  paulmcav
 * attached signals to h/v scale controls
 *
*/

#ifndef _AUDIOFILE_H_
#define _AUDIOFILE_H_

#include <sys/types.h>

/*
*/

const int	ftype_raw =	0x0001;   // raw
const int	ftype_au  =	0x0002;   // au
const int	ftype_voc =	0x0004;   // voc
const int	ftype_wav =	0x0008;   // wave
const int	ftype_mp3 =	0x0010;   // mp3 audio
const int	ftype_ram =	0x1000;   // real media 
const int	ftype_cda =	0x2000;   // CDDA media 

enum _af_format {
	SND_PCM_FORMAT_UNKNOWN = -1,
	SND_PCM_FORMAT_S8, 
	SND_PCM_FORMAT_U8, 
	SND_PCM_FORMAT_S16_LE, 
	SND_PCM_FORMAT_U16_LE, 
	SND_PCM_FORMAT_MU_LAW, 
	SND_PCM_FORMAT_GSM, 
};

class AudioFile {
	protected:
		char *f_name;
		int	  f_type;
		int   f_handle;
		long  f_length;
		int   channels, rate, bps;
		int   samples;
		
	private:
		long  chunklen;
		int	  count;
		char *audiobuf;

		int   format;
		int   vocmajor, vocminor;
		char  f_header[65];

	public:
		AudioFile( const char * );
		~AudioFile();

		const char *name( void ){ return f_name; }
		int	  type( void ){ return f_type; }

		int   getChannels( void ){ return channels; }
		int   getRate( void ){ return rate; }
		int   getBps( void ){ return bps; }
		long  getChunkSamples( void ){ return samples; }
		float getChunkTime( void ){ return getChunkSamples()*1.0/rate; }
		
		int   readSamples( long, int, int * );

		int dump( void );
		
	protected:
		int aclose( void );
		int aopen( void );
		int type_check( void );

	private:
		ssize_t safe_read( int fd, void *buf, size_t count );

		int     test_vocfile(void *buffer);
		ssize_t test_wavefile(int f, char *b, size_t s);
		size_t  test_wavefile_read(int f, char *b, size_t *s, size_t r, int l);
		int     test_au(int f, void *b);
		
};

#endif

