// File:        audium.cc
// Author:      Paul McAvoy <paulmcav@queda.net>
// Org:
// Desc:        
// 
// $Revision$
/*
 * $Log$
 * Revision 1.3  2001/08/13 07:52:29  paulmcav
 * working with drawable area in wave viewer window now
 *
 * Revision 1.2  2001/08/12 06:42:58  paulmcav
 * working version of file selection / format determination
 *
 * Revision 1.1.1.1  2001/08/09 08:04:36  paulmcav
 * import
 *
 */

#include "common.h"
#include "menu.h"

#include <unistd.h>
#include <assert.h>

// ------------------------------------------------------------------
//  Func: 
//  Desc: 
//
//  Ret:  
// ------------------------------------------------------------------

GtkCList *filelist;

// ------------------------------------------------------------------

int
app_init( int argc, char *argv[] )
{
	char c;
	
	// we currently don't have any files open
//	num_files = 0;
//	fd_head = NULL;
	
	while ((c = getopt(argc, argv, "f:?")) != -1) {
		switch (c) {
			case 'f':
				open_new_file( optarg );
				break;
			default:
				break;
		}
	}
	return 0;
}

// ------------------------------------------------------------------
 
int
main( int argc, char *argv[] )
{
	char *list_titles[] =
	{
		{"Filename"},
		{"Ch"},
		{"Bits"},
		{"Rate"},
		{"Len"},
		{"Time"},
	};
	
	GtkWidget *window;
	GtkWidget *main_vbox;
	GtkWidget *menubar;
	GtkWidget *scrollwin;
	GtkWidget *clist;

	gtk_init (&argc, &argv);

	window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_signal_connect (GTK_OBJECT (window), "destroy", 
						GTK_SIGNAL_FUNC (gtk_main_quit), 
						(void*)("WM destroy"));
	gtk_window_set_title (GTK_WINDOW(window), "The Audium");
	gtk_widget_set_usize (GTK_WIDGET(window), 400, 200);

	main_vbox = gtk_vbox_new (FALSE, 1);
	gtk_container_border_width (GTK_CONTAINER (main_vbox), 1);
	gtk_container_add (GTK_CONTAINER (window), main_vbox);
	gtk_widget_show (main_vbox);

	get_main_menu (window, &menubar);
	gtk_box_pack_start (GTK_BOX (main_vbox), menubar, FALSE, TRUE, 0);
	gtk_widget_show (menubar);

	scrollwin = gtk_scrolled_window_new(NULL,NULL);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrollwin),
			GTK_POLICY_AUTOMATIC, GTK_POLICY_ALWAYS );
	gtk_box_pack_start(GTK_BOX(main_vbox), scrollwin, TRUE, TRUE, 0);
	gtk_widget_show( scrollwin );
	
	clist = gtk_clist_new_with_titles(6, list_titles );
	gtk_clist_set_shadow_type (GTK_CLIST(clist), GTK_SHADOW_ETCHED_IN);
	gtk_clist_set_column_width (GTK_CLIST(clist), 0, 250);
	gtk_container_add(GTK_CONTAINER(scrollwin), clist);
	gtk_widget_show( clist );
	
	gtk_widget_show (window);

	// init our app data
	filelist = (GtkCList*)clist;

	app_init( argc, argv );
	
	gtk_main ();

	return 0;
}


