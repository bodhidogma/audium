// File:        AudioData.h
// Author:      Paul McAvoy <paulmcav@queda.net>
// Org:
// Desc:        
//              
// 
// $Revision$

/*
 * $Log$
 * Revision 1.6  2001/08/30 18:10:49  paulmcav
 * attached signals to h/v scale controls
 *
 * Revision 1.5  2001/08/23 06:31:51  paulmcav
 * make a pretty window!
*/

#ifndef _AUDIODATA_H_
#define _AUDIODATA_H_

#include "AudioFile.h"
#include "common.h"

class AudioData : public AudioFile
{
	private:
		GtkWidget *h_window;
		GtkWidget *scrollwin;
		GtkWidget *area;
		GtkCList  *h_main_list;
		int		   main_list_row;

		int        data_ok;
		
		GdkPixmap *pixmap;
		GtkWidget *hruler;

		float   vscale, hscale;
		
	public:
		AudioData( const char * );
		~AudioData();

		int dataOk( void ){ return data_ok; }
		
		int WinSetListRow( GtkCList *, int );

		int WinOpen( void );
		int WinClose( void );

//		int debug( void ){ return dump(); }

	protected:
		int createWindow( void );
		
	private:

		int draw_wave( GtkWidget * );
		
		// list of callbacks to handle stuff for this window
		static void cb_delete( GtkWidget *, gpointer );
		
		void cb_draw_brush( GtkWidget *, gdouble, gdouble );
		
		static gint cb_configure_event( GtkWidget *,
				GdkEventConfigure *, gpointer );
		
		static gint cb_expose_event( GtkWidget *,
				GdkEventExpose *, gpointer );
		
		
		static gint cb_button_press_event( GtkWidget *,
				GdkEventButton *, gpointer );
		
		static gint cb_motion_notify_event( GtkWidget *,
				GdkEventMotion *, gpointer );

		static gint cb_hscale_change_event( GtkAdjustment *, gpointer );
		static gint cb_vscale_change_event( GtkAdjustment *, gpointer );
				
};

#endif


