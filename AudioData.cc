// File:        audioData.cc
// Author:      Paul McAvoy <paulmcav@queda.net>
// Org:
// Desc:        
// 
// $Revision$
/*
 * $Log$
 * Revision 1.11  2001/08/30 18:10:49  paulmcav
 * attached signals to h/v scale controls
 *
 * Revision 1.10  2001/08/23 06:31:51  paulmcav
 * make a pretty window!
 */


#include <assert.h>
#include <iostream.h>
#include <math.h>
#include <stdlib.h>

#include "AudioData.h"

// ------------------------------------------------------------------
//  Func: 
//  Desc: 
//
//  Ret:  
// ------------------------------------------------------------------

AudioData::AudioData( const char *file )
   	: AudioFile( file )
	, h_window( 0 )
	, scrollwin( 0 )
	, h_main_list( 0 )
	, main_list_row( 0 )
	, data_ok( 0 )
	, pixmap( 0 )
	, vscale( 0 )
	, hscale( 0 )
{
	// notify that there is data available
	if (f_handle > 0 && channels) {
		data_ok = 1;
	}
	else {
		cerr <<" problem reading file data" << endl;
   	}
}

AudioData::~AudioData( )
{
	int clist_row;
	
	if ( h_main_list != 0 ){
		// find our row in the list, and delete it (main win)
		clist_row = gtk_clist_find_row_from_data(
				h_main_list, (gpointer)main_list_row
				);
		gtk_clist_remove( h_main_list, clist_row );
	}
}

int
AudioData::WinSetListRow( GtkCList *hlist, int rownum )
{
	main_list_row = rownum;
	h_main_list = hlist;

	// go and store something significant about our row
	// so we can find it later
	gtk_clist_set_row_data( hlist, rownum, (gpointer)main_list_row );
	
	return main_list_row;
}


int
AudioData::WinOpen( void )
{
	return createWindow();
}

int
AudioData::WinClose( void )
{
	return 0;
}

/* Backing pixmap for drawing area */
//GdkPixmap *pixmap = NULL;

/* Draw a rectangle on the screen */
void
AudioData::cb_draw_brush( GtkWidget *widget, gdouble x, gdouble y)
{
  GdkRectangle update_rect;

  update_rect.x = (int)x - 1;
  update_rect.y = (int)y - 1;
  update_rect.width = 2;
  update_rect.height = 2;
  gdk_draw_rectangle (pixmap,
                      widget->style->black_gc,
                      TRUE,
                      update_rect.x, update_rect.y,
                      update_rect.width, update_rect.height);
  gtk_widget_draw (widget, &update_rect);
}

/* Create a new backing pixmap of the appropriate size */
gint
AudioData::cb_configure_event( GtkWidget *area, GdkEventConfigure *event, gpointer data )
{
	AudioData *ad = (AudioData*)data;
	
	if (ad->pixmap)
		gdk_pixmap_unref(ad->pixmap);

//	cout <<" w: "<< widget->allocation.width <<" \th: "<< widget->allocation.height << endl;
	
	ad->pixmap = gdk_pixmap_new(area->window,
                          area->allocation.width,
                          area->allocation.height,
                          -1);
	
	gdk_draw_rectangle (ad->pixmap,
                      area->style->black_gc,
                      TRUE,
                      0, 0,
                      area->allocation.width,
                      area->allocation.height);
	
	ad->draw_wave( area );

	return TRUE;
}

// widget == class.area

int
AudioData::draw_wave( GtkWidget *widget )
{
	GdkGC *c_l, *c_r;
	GdkColor *color;

	int w, h;
	int l, r;
	int cnt; 

	int data[2] = { 0 };
	int px, ply, pry;
	int s;
	int ws, hs;

	int lmax, rmax;
	
	w = widget->allocation.width;
	h = widget->allocation.height;

	// find left right center positions;
	l = h / 4;
	r = l*3;
	
	// calculate scale to show whole wav on screen
	ws = samples / w;
	hs = (int)pow(2,15) / l;
	
	cout <<" w: "<< w <<" h: "<< h <<" l: "<< l
	     <<" len: " << samples <<" ws: " << ws <<" hs: " << hs
	     << endl;

	c_l = gdk_gc_new( widget->window );
	c_r = gdk_gc_new( widget->window );
	
#define COLORVAL(x)     (65535/256)*x
#define PIXELCOL(r,g,b)	(r*65535 + g*256 + b)	
	
	color = new GdkColor;
	
	color->red = 0;
	color->blue = 0;
	color->green = COLORVAL( 255 );
	color->pixel = PIXELCOL( 0,0,255 );
	gdk_color_alloc( gtk_widget_get_colormap(widget), color);
	gdk_gc_set_foreground( c_l, color );
	
	color->red = COLORVAL( 255 );
	color->blue = 0;
	color->green = 0;
	color->pixel = PIXELCOL( 255,0,0 );
	gdk_color_alloc( gtk_widget_get_colormap(widget), color);
	gdk_gc_set_foreground( c_r, color );
	
	gdk_draw_line( pixmap,
			widget->style->white_gc,
			0, l,
			w, l );
	gdk_draw_line( pixmap,
			widget->style->white_gc,
			0, r,
			w, r );

	s = 0;
	
	lmax = 0;
	rmax = 0;
	
	px = 0;
	ply = l;
   	pry = r;
	
	for ( cnt=0; cnt<w; cnt++ ){
		s = cnt * ws; 
		readSamples( (int)s, 2, &data[0] );

		if ( lmax < abs(data[0]) ) lmax = abs(data[0]);
		if ( rmax < abs(data[1]) ) rmax = abs(data[1]);
		
		data[0] /= hs;
		data[1] /= hs;

		gdk_draw_line( pixmap,
//			widget->style->white_gc,
				c_l,
				px, ply,
				cnt, l+data[0] );
		gdk_draw_line( pixmap,
//			widget->style->white_gc,
				c_r,
				px, pry,
				cnt, r+data[1] );
		
		px = cnt;
		ply = l+data[0];
		pry = r+data[1];		
		
// cout <<"c: "<< cnt <<" \ts:"<< s <<" \tl: "<< ls << " \tr: "<< rs <<endl;
	}
//	cout <<" lmax: "<< lmax <<" rmax: "<< rmax <<" t: "<< (s/rate) << endl;
	cout <<" cnt: "<< cnt <<" s: "<< s << endl;

//	gtk_ruler_set_range( h_ruler, 0, s/1000, 0, s/1000 );
	
	delete color;
	return 0;
}

/* Redraw the screen from the backing pixmap */
gint
AudioData::cb_expose_event( GtkWidget *widget, GdkEventExpose *event, gpointer data )
{
	AudioData *ad = (AudioData*)data;

	gdk_draw_pixmap(widget->window,
                  widget->style->fg_gc[GTK_WIDGET_STATE (widget)],
                  ad->pixmap,
                  event->area.x, event->area.y,
                  event->area.x, event->area.y,
                  event->area.width, event->area.height);

	return FALSE;
}

int
AudioData::cb_button_press_event( GtkWidget *widget, GdkEventButton *event, gpointer data )
{
	AudioData *ad = (AudioData*)data;
  if (event->button == 1 && ad->pixmap != NULL)
    ad->cb_draw_brush( widget, event->x, event->y );

  return TRUE;
}

gint
AudioData::cb_motion_notify_event( GtkWidget *widget, GdkEventMotion *event, gpointer data )
{
	AudioData *ad = (AudioData*)data;
	int x, y;
	GdkModifierType state;

	if (event->is_hint)
		gdk_window_get_pointer (event->window, &x, &y, &state);
	else
	{
		x = (int)event->x;
		y = (int)event->y;
		state = (GdkModifierType)event->state;
	}

	if (state & GDK_BUTTON1_MASK && ad->pixmap != NULL)
		ad->cb_draw_brush( widget, x, y );

	return TRUE;
}

int
AudioData::cb_hscale_change_event( GtkAdjustment *adj, gpointer data )
{
	AudioData *ad = (AudioData*)data;

	ad->hscale = adj->value;

	return TRUE;
}

int
AudioData::cb_vscale_change_event( GtkAdjustment *adj, gpointer data )
{
	AudioData *ad = (AudioData*)data;

	ad->vscale = adj->value;

	return TRUE;
}

#define EVENT_METHOD(i, x) GTK_WIDGET_CLASS(GTK_OBJECT(i)->klass)->x

int
AudioData::createWindow( void )
{
  GtkWidget *table;
  GtkWidget *label7;
  GtkWidget *label8;
  GtkWidget *hs_vscale;
  GtkWidget *viewport1;
  GtkWidget *vbox2;
//  GtkWidget *area;
  GtkWidget *hs_hscale;
  GtkObject *adj;

  h_window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_widget_set_name (h_window, "h_window");
  gtk_object_set_data (GTK_OBJECT (h_window), "h_window", h_window);
	gtk_window_set_title( GTK_WINDOW(h_window), f_name );
  gtk_window_set_default_size (GTK_WINDOW (h_window), 600, 400);

  table = gtk_table_new (3, 6, FALSE);
  gtk_widget_set_name (table, "table");
  gtk_widget_ref (table);
  gtk_object_set_data_full (GTK_OBJECT (h_window), "table", table,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (table);
  gtk_container_add (GTK_CONTAINER (h_window), table);

  label7 = gtk_label_new ("V Scale");
  gtk_widget_set_name (label7, "label7");
  gtk_widget_ref (label7);
  gtk_object_set_data_full (GTK_OBJECT (h_window), "label7", label7,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (label7);
  gtk_table_attach (GTK_TABLE (table), label7, 1, 2, 0, 1,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_misc_set_alignment (GTK_MISC (label7), 0.9, 0.5);

  label8 = gtk_label_new ("H Scale");
  gtk_widget_set_name (label8, "label8");
  gtk_widget_ref (label8);
  gtk_object_set_data_full (GTK_OBJECT (h_window), "label8", label8,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (label8);
  gtk_table_attach (GTK_TABLE (table), label8, 3, 4, 0, 1,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_label_set_justify (GTK_LABEL (label8), GTK_JUSTIFY_RIGHT);
  gtk_misc_set_alignment (GTK_MISC (label8), 0.9, 0.5);

	adj = gtk_adjustment_new (1, 0.2, 2, 0.1, 0.2, 0);
  hs_vscale = gtk_hscale_new (GTK_ADJUSTMENT (adj));
  gtk_widget_set_name (hs_vscale, "hs_vscale");
  gtk_widget_ref (hs_vscale);
  gtk_object_set_data_full (GTK_OBJECT (h_window), "hs_vscale", hs_vscale,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (hs_vscale);
  gtk_table_attach (GTK_TABLE (table), hs_vscale, 2, 3, 0, 1,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (GTK_FILL), 0, 0);
  gtk_scale_set_value_pos (GTK_SCALE (hs_vscale), GTK_POS_LEFT);

  gtk_signal_connect(GTK_OBJECT(adj),"value_changed",
		  GTK_SIGNAL_FUNC(cb_vscale_change_event), this );
  
  scrollwin = gtk_scrolled_window_new (NULL, NULL);
  gtk_widget_set_name (scrollwin, "scrollwin");
  gtk_widget_ref (scrollwin);
  gtk_object_set_data_full (GTK_OBJECT (h_window), "scrollwin", scrollwin,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (scrollwin);
  gtk_table_attach (GTK_TABLE (table), scrollwin, 1, 6, 1, 3,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL), 0, 0);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrollwin),
		  GTK_POLICY_ALWAYS, GTK_POLICY_AUTOMATIC);

  viewport1 = gtk_viewport_new (NULL, NULL);
  gtk_widget_set_name (viewport1, "viewport1");
  gtk_widget_ref (viewport1);
  gtk_object_set_data_full (GTK_OBJECT (h_window), "viewport1", viewport1,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (viewport1);
  gtk_container_add (GTK_CONTAINER (scrollwin), viewport1);

  vbox2 = gtk_vbox_new (FALSE, 0);
  gtk_widget_set_name (vbox2, "vbox2");
  gtk_widget_ref (vbox2);
  gtk_object_set_data_full (GTK_OBJECT (h_window), "vbox2", vbox2,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (vbox2);
  gtk_container_add (GTK_CONTAINER (viewport1), vbox2);

  hruler = gtk_hruler_new ();
  gtk_widget_set_name (hruler, "hruler");
  gtk_widget_ref (hruler);
  gtk_object_set_data_full (GTK_OBJECT (h_window), "hruler", hruler,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (hruler);
  gtk_box_pack_start (GTK_BOX (vbox2), hruler, FALSE, TRUE, 0);

  area = gtk_drawing_area_new ();
  gtk_widget_set_name (area, "area");
  gtk_widget_ref (area);
  gtk_object_set_data_full (GTK_OBJECT (h_window), "area", area,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (area);
  gtk_box_pack_start (GTK_BOX (vbox2), area, TRUE, TRUE, 0);

  adj = gtk_adjustment_new( 1, 0.2, 2, 0.1, 0.2, 0);
  hs_hscale = gtk_hscale_new (GTK_ADJUSTMENT (adj));
  gtk_widget_set_name (hs_hscale, "hs_hscale");
  gtk_widget_ref (hs_hscale);
  gtk_object_set_data_full (GTK_OBJECT (h_window), "hs_hscale", hs_hscale,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (hs_hscale);
  gtk_table_attach (GTK_TABLE (table), hs_hscale, 4, 5, 0, 1,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (GTK_FILL), 0, 0);
  gtk_scale_set_value_pos (GTK_SCALE (hs_hscale), GTK_POS_LEFT);

  gtk_signal_connect(GTK_OBJECT(adj),"value_changed",
		  GTK_SIGNAL_FUNC(cb_hscale_change_event), this );
  
	
	// signal when window is nixd
	gtk_signal_connect( GTK_OBJECT(h_window), "destroy",
			GTK_SIGNAL_FUNC(cb_delete), this );
	
	// attach signals to drawing area events
	gtk_drawing_area_size( GTK_DRAWING_AREA(area),400,200 );
	gtk_signal_connect( GTK_OBJECT(area), "configure_event",
			GTK_SIGNAL_FUNC(cb_configure_event), this );
	
	gtk_signal_connect( GTK_OBJECT(area), "expose_event",
			GTK_SIGNAL_FUNC(cb_expose_event), this );
	
	gtk_signal_connect( GTK_OBJECT (area), "button_press_event",
			(GtkSignalFunc)cb_button_press_event, this );
	gtk_signal_connect( GTK_OBJECT (area), "motion_notify_event",
			(GtkSignalFunc)cb_motion_notify_event, this );

	gtk_widget_set_events( area, GDK_EXPOSURE_MASK
                         | GDK_LEAVE_NOTIFY_MASK
                         | GDK_BUTTON_PRESS_MASK
                         | GDK_POINTER_MOTION_MASK
                         | GDK_POINTER_MOTION_HINT_MASK );
	
	// set up our funny ruler!
	gtk_ruler_set_range( GTK_RULER(hruler), 0, getChunkTime(), 0, getChunkTime() );
	gtk_signal_connect_object( GTK_OBJECT(area), "motion_notify_event",
			(GtkSignalFunc)EVENT_METHOD(hruler, motion_notify_event),
			GTK_OBJECT(hruler) );

	// finalize
	gtk_widget_show( h_window );
	
	return 0;
}


void
AudioData::cb_delete( GtkWidget *obj, gpointer data )
{
	AudioData *ad = (AudioData*)data;
	delete ad;
}
