// File:        test.cc
// Author:      Paul McAvoy <paulmcav@queda.net>
// Org:
// Desc:        
// 
// $Revision$
/*
 * $Log$
 * Revision 1.3  2001/08/12 06:42:58  paulmcav
 * working version of file selection / format determination
 *
 * Revision 1.2  2001/08/10 06:33:34  paulmcav
 * mods
 *
 * Revision 1.1.1.1  2001/08/09 08:04:44  paulmcav
 * import
 *
 */


// ------------------------------------------------------------------
//  Func: 
//  Desc: 
//
//  Ret:  
// ------------------------------------------------------------------

#include <iostream.h>

#include "AudioData.h"


int
main( int argc, char *argv[] )
{
	AudioFile	af( "data/ding.wav" );

	return 0;
}

