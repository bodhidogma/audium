// File:        AudioFile.cc
// Author:      Paul McAvoy <paulmcav@queda.net>
// Org:
// Desc:        
// 
// $Revision$
/*
 * $Log$
 * Revision 1.10  2001/08/30 18:10:49  paulmcav
 * attached signals to h/v scale controls
 *
 */

#include "AudioFile.h"
#include "formats.h"

#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <iostream.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <errno.h>



// ------------------------------------------------------------------
//  Func: 
//  Desc: 
//
//  Ret:  
// ------------------------------------------------------------------

AudioFile::AudioFile( const char *file )
	: f_name( NULL )
	, f_type( 0 )
	, f_handle( 0 )
	, f_length( 0 )
	, channels( 0 ), rate( 0 ), bps( 0 )
	, samples(0), chunklen( 0 )
	, count( 0 ), audiobuf( NULL )
	, format( 0 )
	, vocmajor( 0 ), vocminor( 0 )
{
	int len;

	assert( file );
	
	len = strlen(file);
	
	f_name = new char[ len+1 ];
	assert( f_name );

	strncpy( f_name, file, len+1 );

	audiobuf = new char[ 1024 ];
	assert( audiobuf );
	
	aopen();
}

AudioFile::~AudioFile( )
{
	aclose();
	
	if ( f_name ) delete f_name;
}

int
AudioFile::aclose( void )
{
	if ( f_handle > 0){
	   	close( f_handle );
		if ( munmap( audiobuf, f_length ) == -1 ){
			perror( "munmap failed:" );
		}
	}
	
	f_type = 0;
	f_handle = 0;
	f_length = 0;
	channels = 0;
	rate = 0;
	bps = 0;
	samples = 0;
	chunklen = 0;

	audiobuf = 0;
		
	return 0;
}

int
AudioFile::aopen( void )
{
	long fpos;

	type_check();

	if ( f_handle > 0 ){
		// get file length in bytes
		fpos = lseek( f_handle, 0, SEEK_END );
		f_length = fpos;

//		cerr <<" fpos: "<< fpos <<" len: "<< chunklen << endl;
		if ( (int)(audiobuf = (char*)mmap( 0, fpos, 
			   	PROT_READ, MAP_PRIVATE, f_handle, 0)) == -1 ){
			perror("mmap failed");
			cout <<" errno: " << errno << endl;
			audiobuf = 0;
		}

	}
	return 0;
}

// ------------------------------------------------------------------

int
AudioFile::type_check( void )
{
	int fd, ofs;
	size_t dta;
	ssize_t dtawave;

	if (f_name == NULL)
		return -1;
	
	f_type = 0;
	
	if ((fd = open(f_name, O_RDONLY, 0)) == -1) {
		perror( f_name );
		aclose();
		return( -2 );
	}
	
	dta = sizeof( AuHeader );
	if (safe_read(fd, audiobuf, dta) != (int)dta) {
		cerr << "read error" << endl;
		aclose();
		return( -2 );
	}

	if (test_au(fd, audiobuf) >= 0) {
		format = SND_PCM_FORMAT_MU_LAW;
		f_type = ftype_au;
//		playback_go
		goto __end;
	}
	dta = sizeof(VocHeader);
	if (safe_read(fd, audiobuf + sizeof(AuHeader), dta - sizeof(AuHeader))
		   	!= (int)dta - (int)sizeof(AuHeader)) {
		cerr << "read error" << endl;
		aclose();
		return( -2 );
	}
	if ((ofs = test_vocfile(audiobuf)) >= 0) {
		f_type = ftype_voc;
//		voc_play(fd
		goto __end;
	}
	if ((dtawave = test_wavefile(fd, audiobuf, dta)) >= 0) {
		f_type = ftype_wav;
//		playback_go
	}
	else {
		f_type = ftype_raw;
		// init_raw_data();
		// playback_go
	}

__end:
	f_handle = fd;
			
	// other misc length info
	samples = chunklen / channels / (bps/8);
		
	return f_type;
}

// ------------------------------------------------------------------

ssize_t
AudioFile::safe_read( int fd, void *buf, size_t count )
{
	ssize_t result = 0, res;

	while (count > 0) {
		if ((res = read(fd, buf, count)) == 0)
			break;
		if (res < 0)
			return result > 0 ? result : res;
		count -= res;
		result += res;
		(char *)buf += res;
	}
	return result;
}

// ------------------------------------------------------------------

/*
 * Test, if it is a .VOC file and return >=0 if ok (this is the length of rest)
 *                                       < 0 if not 
 */
int
AudioFile::test_vocfile(void *buffer)
{
	VocHeader *vp = (VocHeader*)buffer;

	if (strstr((const char*)vp->magic, VOC_MAGIC_STRING)) {
		vocminor = vp->version & 0xFF;
		vocmajor = vp->version / 256;
		if (vp->version != (0x1233 - vp->coded_ver))
			return -2;	/* coded version mismatch */

		channels = 1;
		rate = 8000;
		bps = 8;
		
		return vp->headerlen - sizeof(VocHeader);	/* 0 mostly */
	}
	return -1;		/* magic string fail */
}

// ------------------------------------------------------------------

size_t
AudioFile::test_wavefile_read(int fd, char *buffer, size_t *size
		, size_t reqsize, int line)
{
	if (*size >= reqsize)
		return *size;
	if (safe_read(fd, buffer + *size, reqsize - *size)
		   	!= (int)reqsize - (int)*size) {

		fprintf( stderr,"read error (called from line %i)", line);
//		error("read error (called from line %i)", line);
		return 0;
	}
	return *size = reqsize;
}

/*
 * test, if it's a .WAV file, > 0 if ok (and set the speed, stereo etc.)
 *                            == 0 if not
 * Value returned is bytes to be discarded.
 */
ssize_t
AudioFile::test_wavefile(int fd, char *buffer, size_t size)
{
	WaveHeader *h = (WaveHeader *)buffer;
	WaveFmtBody *f;
	WaveChunkHeader *c;
	u_int type, len;

	if (size < sizeof(WaveHeader))
		return -1;
	if (h->magic != WAV_RIFF || h->type != WAV_WAVE)
		return -1;
	if (size > sizeof(WaveHeader))
		memmove(buffer, buffer + sizeof(WaveHeader), size - sizeof(WaveHeader));
	size -= sizeof(WaveHeader);
	while (1) {
		test_wavefile_read(fd, buffer, &size, sizeof(WaveChunkHeader), __LINE__);
		c = (WaveChunkHeader*)buffer;
		type = c->type;
		len = LE_INT(c->length);
		if (size > sizeof(WaveChunkHeader))
			memmove(buffer, buffer + sizeof(WaveChunkHeader), size - sizeof(WaveChunkHeader));
		size -= sizeof(WaveChunkHeader);
		if (type == WAV_FMT)
			break;
		test_wavefile_read(fd, buffer, &size, len, __LINE__);
		if (size > len)
			memmove(buffer, buffer + len, size - len);
		size -= len;
	}

	if (len < sizeof(WaveFmtBody)) {
		fprintf(stderr,"unknown length of 'fmt ' chunk (read %u, should be %u at least)", len, (u_int)sizeof(WaveFmtBody));
//		error("unknown length of 'fmt ' chunk (read %u, should be %u at least)", len, (u_int)sizeof(WaveFmtBody));
		aclose();
		return( -2 );
	}
	test_wavefile_read(fd, buffer, &size, len, __LINE__);
	f = (WaveFmtBody*) buffer;
	if (LE_SHORT(f->format) != WAV_PCM_CODE) {
		fprintf(stderr,"can't play not PCM-coded WAVE-files");
//		error("can't play not PCM-coded WAVE-files");
		aclose();
		return( -2 );
	}
	if (LE_SHORT(f->modus) < 1) {
		fprintf(stderr,"can't play WAVE-files with %d tracks", LE_SHORT(f->modus));
//		error("can't play WAVE-files with %d tracks", LE_SHORT(f->modus));
		aclose();
		return( -2 );
	}
	channels = LE_SHORT(f->modus);
	bps = LE_SHORT(f->bit_p_spl);
	switch (LE_SHORT(f->bit_p_spl)) {
	case 8:
		format = SND_PCM_FORMAT_U8;
		break;
	case 16:
		format = SND_PCM_FORMAT_S16_LE;
		break;
	default:
		fprintf(stderr," can't play WAVE-files with sample %d bits wide", LE_SHORT(f->bit_p_spl));
//		error(" can't play WAVE-files with sample %d bits wide", LE_SHORT(f->bit_p_spl));
		aclose();
		return( -2 );
	}
	rate = LE_INT(f->sample_fq);
	
	if (size > len)
		memmove(buffer, buffer + len, size - len);
	size -= len;
	
	while (1) {
		u_int type, len;

		test_wavefile_read(fd, buffer, &size, sizeof(WaveChunkHeader), __LINE__);
		c = (WaveChunkHeader*)buffer;
		type = c->type;
		len = LE_INT(c->length);
		chunklen = LE_INT(c->length);
		if (size > sizeof(WaveChunkHeader))
			memmove(buffer, buffer + sizeof(WaveChunkHeader), size - sizeof(WaveChunkHeader));
		size -= sizeof(WaveChunkHeader);
		if (type == WAV_DATA) {
			if ((int)len < count)
				count = len;
			return size;
		}
		test_wavefile_read(fd, buffer, &size, len, __LINE__);
		if (size > len)
			memmove(buffer, buffer + len, size - len);
		size -= len;
	}

	/* shouldn't be reached */
	return -1;
}

// ------------------------------------------------------------------

int
AudioFile::test_au(int fd, void *buffer)
{
	AuHeader *ap = (AuHeader*)buffer;

	if (ap->magic != AU_MAGIC)
		return -1;
	if (BE_INT(ap->hdr_size) > 128 || BE_INT(ap->hdr_size) < 24)
		return -1;
	count = BE_INT(ap->data_size);
	switch (BE_INT(ap->encoding)) {
	case AU_FMT_ULAW:
		format = SND_PCM_FORMAT_MU_LAW;
		bps = 8;
		break;
	case AU_FMT_LIN8:
		format = SND_PCM_FORMAT_U8;
		bps = 8;
		break;
	case AU_FMT_LIN16:
		format = SND_PCM_FORMAT_U16_LE;
		bps = 16;
		break;
	default:
		return -1;
	}
	rate = BE_INT(ap->sample_rate);
	if (rate < 2000 || rate > 256000)
		return -1;
	channels = BE_INT(ap->channels);
	if (channels < 1 || channels > 128)
		return -1;
	if (safe_read(fd,
			(void*)((long)buffer + sizeof(AuHeader))
			, BE_INT(ap->hdr_size)
		   	- (sizeof(AuHeader) != BE_INT(ap->hdr_size))
		   	- sizeof(AuHeader))
			)
   	{
		cerr <<"read error"<< endl;
		aclose();
		return( -2 );
	}
	return 0;
}

// ------------------------------------------------------------------
 
int
AudioFile::dump( void )
{
	cout <<"file: "<< f_name << endl;
	cout <<"type: "<< f_type
	     <<" \tchan: "<< channels
	     <<" \trate: "<< rate
	     <<" \tbps: "<< bps 
	     <<" \tlen: "<< chunklen 
		 <<endl;

	return 0;
}

// ------------------------------------------------------------------

int
AudioFile::readSamples( long samp_pos, int ch_cnt, int *ch )
{
	char *ptr;
	int  pstep, cnt;
	short sample;

	if ( audiobuf == 0 )
		return -1;
	
	pstep = bps / 8;
	
	// start of our audio data
	ptr = audiobuf + 44 + (pstep * samp_pos);

	cnt = 0;
	while ( (cnt < ch_cnt) && (ptr-audiobuf) < f_length){
		memcpy( &sample, ptr, sizeof(short) );
		ch[ cnt ] = sample;
		ptr += pstep;
		cnt++;
	}
	return 0;
}
