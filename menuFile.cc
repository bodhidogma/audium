// File:        menuFile.c
// Author:      Paul McAvoy <paulmcav@queda.net>
// Org:
// Desc:        
// 
// $Revision$
/*
 * $Log$
 * Revision 1.3  2001/08/13 07:52:29  paulmcav
 * working with drawable area in wave viewer window now
 *
 * Revision 1.2  2001/08/12 06:42:58  paulmcav
 * working version of file selection / format determination
 *
 * Revision 1.1.1.1  2001/08/09 08:04:35  paulmcav
 * import
 *
 */


#include "common.h"

#include <stdio.h>
#include <string.h>
#include <assert.h>

#include "AudioData.h"
#include "menuFile.h"

// ------------------------------------------------------------------
//  Func: 
//  Desc: 
//
//  Ret:  
// ------------------------------------------------------------------

extern GtkCList    *filelist;

int
open_new_file( const char *file_name )
{
//	gchar *data[5] = { {"one"},{"two"},{"three"},{"four"},{"five"} };
	gchar *lptr[6];
	gchar buff[6][255];

	memset( lptr, '\0', sizeof( lptr ) );
	memset( buff, '\0', sizeof( buff ) );
	
	AudioData *ad;
	
	ad = new AudioData( file_name );
	assert( ad );
	
	if ( !ad->dataOk() ){
		g_message( "open_new_file: PROBLEM! %s", file_name );
		delete ad;
		return -1;
	}
	
	strcpy( buff[0], file_name );
	sprintf( buff[1],"%d", ad->getChannels() );
	sprintf( buff[2],"%d", ad->getBps() );
	sprintf( buff[3],"%d Hz", ad->getRate() );
	sprintf( buff[4],"%ld", ad->getChunkSamples() );
	sprintf( buff[5],"%.2f s", ad->getChunkTime() );

	lptr[0] = buff[0];
	lptr[1] = buff[1];
	lptr[2] = buff[2];
	lptr[3] = buff[3];
	lptr[4] = buff[4];
	lptr[5] = buff[5];
	
	ad->WinSetListRow( filelist
			, gtk_clist_append( filelist, lptr )
			);

	gtk_clist_columns_autosize( filelist );

	ad->WinOpen();
	
	return 0;
}

