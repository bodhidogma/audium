// File:        menu.h
// Author:      Paul McAvoy <paulmcav@queda.net>
// Org:
// Desc:        
//              
// 
// $Revision$

/*
 * $Log$
 * Revision 1.2  2001/08/12 06:42:58  paulmcav
 * working version of file selection / format determination
 *
 * Revision 1.1.1.1  2001/08/09 08:04:44  paulmcav
 * import
 *
 * 
*/

#ifndef _MENU_H_
#define _MENU_H_

#include "common.h"
#include "menuFile.h"

/*
*/

void get_main_menu( GtkWidget  *window, GtkWidget **menubar );


#endif


