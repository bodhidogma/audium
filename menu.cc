// File:        menu.cc
// Author:      Paul McAvoy <paulmcav@queda.net>
// Org:
// Desc:        
// 
// $Revision$
/*
 * $Log$
 * Revision 1.1  2001/08/09 08:04:35  paulmcav
 * Initial revision
 *
 */


#include "common.h"
#include "menuFile.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

// ------------------------------------------------------------------
//  Func: 
//  Desc: 
//
//  Ret:  
// ------------------------------------------------------------------

void
m_about( GtkWidget *w, gpointer data )
{
	g_message ("Help | About");
}

// ------------------------------------------------------------------
//  Func: 
//  Desc: 
//
//  Ret:  
// ------------------------------------------------------------------

void
file_OkSel( GtkWidget *w, GtkFileSelection *fs )
{
	int fhandle;
	gchar *fname;

	fname = gtk_file_selection_get_filename( GTK_FILE_SELECTION( fs ));
	
	fhandle = open( fname, O_RDONLY );

	// if the file handle open's okay
	if ( fhandle > 0 ) {
		close( fhandle );
		open_new_file( fname );
		
		// I found the top level parent, lets destroy it!
		gtk_widget_destroy( w->parent->parent->parent );
	}
	else {
		g_print( "file-select: %s\n", fname );
	}
}

void
m_fileAction( GtkWidget *w, gpointer data )
{
	GtkWidget *filew;
	int  option = (int)data;
	
	gchar *title = NULL;

	switch ( option ){
//		case 1:			// new
//			break;
		case 2:			// open
			title = "Open File";
			break;
//		case 3:			// save
//			beak;
//		case 4:			// save as
//			break;
		default:
			option = 0;
	g_message ("File Menu action: %d", (int)(data));
	}

	if ( option ){
		filew = gtk_file_selection_new( title );
		
//		gtk_signal_connect( GTK_OBJECT(filew), "destroy",
//				(GtkSignalFunc) destroy, &filew );

		gtk_signal_connect(
			   	GTK_OBJECT( GTK_FILE_SELECTION( filew )->ok_button),
				"clicked", (GtkSignalFunc) file_OkSel, filew
			   	);
		
		gtk_signal_connect_object(
				GTK_OBJECT (GTK_FILE_SELECTION(filew)->cancel_button),
				"clicked", (GtkSignalFunc) gtk_widget_destroy,
				GTK_OBJECT (filew)
				);

		gtk_widget_show( filew );
	}
}

/* This is the GtkItemFactoryEntry structure used to generate new menus.
   Item 1: The menu path. The letter after the underscore indicates an
           accelerator key once the menu is open.
   Item 2: The accelerator key for the entry
   Item 3: The callback function.
   Item 4: The callback action.  This changes the parameters with
           which the function is called.  The default is 0.
   Item 5: The item type, used to define what kind of an item it is.
           Here are the possible values:

           NULL               -> "<Item>"
           ""                 -> "<Item>"
           "<Title>"          -> create a title item
           "<Item>"           -> create a simple item
           "<CheckItem>"      -> create a check item
           "<ToggleItem>"     -> create a toggle item
           "<RadioItem>"      -> create a radio item
           <path>             -> path of a radio item to link against
           "<Separator>"      -> create a separator
           "<Branch>"         -> create an item to hold sub items (optional)
           "<LastBranch>"     -> create a right justified branch 
*/

#define GIFC	GtkItemFactoryCallback

static GtkItemFactoryEntry menu_items[] = {
  { "/_File",         NULL,         NULL, 0, "<Branch>" },
//  { "/File/_New",     "<control>N", (GIFC)m_fileAction, 1, NULL },
  { "/File/_Open",    "<control>O", (GIFC)m_fileAction, 2, NULL },
  { "/File/_Save",    "<control>S", (GIFC)m_fileAction, 3, NULL },
  { "/File/Save _As", NULL,         (GIFC)m_fileAction, 4, NULL },
  { "/File/sep1",     NULL,         NULL, 0, "<Separator>" },
  { "/File/Quit",     "<control>Q", gtk_main_quit, 0, NULL },
//  { "/_Options",      NULL,         NULL, 0, "<Branch>" },
//  { "/Options/Test",  NULL,         NULL, 0, NULL },
  { "/_Help",         NULL,         NULL, 0, "<LastBranch>" },
  { "/_Help/About",   NULL,         (GIFC)m_about, 0, NULL },
};

//#pragma strict

void get_main_menu( GtkWidget  *window,
                    GtkWidget **menubar )
{
  GtkItemFactory *item_factory;
  GtkAccelGroup *accel_group;
  gint nmenu_items = sizeof (menu_items) / sizeof (menu_items[0]);

  accel_group = gtk_accel_group_new ();

  /* This function initializes the item factory.
     Param 1: The type of menu - can be GTK_TYPE_MENU_BAR, GTK_TYPE_MENU,
              or GTK_TYPE_OPTION_MENU.
     Param 2: The path of the menu.
     Param 3: A pointer to a gtk_accel_group.  The item factory sets up
              the accelerator table while generating menus.
  */

  item_factory = gtk_item_factory_new (GTK_TYPE_MENU_BAR, "<main>", 
                                       accel_group);

  /* This function generates the menu items. Pass the item factory,
     the number of items in the array, the array itself, and any
     callback data for the the menu items. */
  gtk_item_factory_create_items (item_factory, nmenu_items, menu_items, NULL);

  /* Attach the new accelerator group to the window. */
  gtk_window_add_accel_group (GTK_WINDOW (window), accel_group);

  if (menubar)
    /* Finally, return the actual menu bar created by the item factory. */ 
    *menubar = gtk_item_factory_get_widget (item_factory, "<main>");
}

