# $Id$
#
##### MACROS #####

#debugging!
#MALLOC	= -static -lefence
#PROF = -pg
#OPTIMIZE = -O3 -funroll-loops

GTK_LIBS=`gtk-config --libs`
GTK_CFLAGS=`gtk-config --cflags`

ALSA_LIBS=-L/opt/lib -lasound

CC	= gcc
CXX = g++
CFLAGS	= $(INCDIR) -Wall -g $(PROF) $(OPTIMIZE) $(GTK_CFLAGS)
LD	= $(CC)
LDFLAGS	= $(PROF) $(GTK_LIBS) -lstdc++
LIBS	= $(MALLOC)
OBJ	= obj/

##### RULES #####

VPATH	= .:$(OBJ)

.SUFFIXES:

define make-dep
	@rm -f $@
	@$(SHELL) -ec '$(CC) -MM $(CPPFLAGS) $(CFLAGS) $< | \
	sed -e "1s|^|$(OBJ)|" -e "s|:| $@:|" \
	> $@; \
	[ -s $@ ] || rm -f $@'
endef

$(OBJ)%.o: %.cc
	$(CXX) $(CFLAGS) -c $< -o $@

$(OBJ)%.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@

$(OBJ)%.cc.d: %.cc
	$(make-dep)

$(OBJ)%.c.d: %.c
	$(make-dep)

##### TARGETS #####

TARGETS= audium

all: do-it-all

ifeq (.depend,$(wildcard .depend))
include .depend
do-it-all: $(TARGETS)
else
do-it-all: dep 
endif

APP	= audium.o menu.o menuFile.o AudioData.o AudioFile.o
TEST = test.o audioData.o AudioFile.o

audium: ${addprefix $(OBJ),$(APP)}
	$(LD) $(LDFLAGS) $^ $(LIBS) -o $@

test: ${addprefix $(OBJ),$(TEST)}
	$(LD) $(LDFLAGS) $^ $(LIBS) -o $@

sources	=$(wildcard *.c*)
# $(wildcard *.cc)

dep	: ${addprefix $(OBJ),${addsuffix .d,$(sources)}}
	@echo Creating dependencies.
	@echo "include $^" > .depend

tags	:
	@ctags -f .tags *.c* *.h

cleanbak:
	find . -name "*~" -exec rm -f '{}' ';'
	
clean	: cleanbak
	rm -f core $(OBJ)*.o

semiclean: clean
	rm -f .depend
	rm -f $(OBJ)*.d
	-strip $(TARGETS)

realclean: semiclean
	rm -f $(TARGETS)

dist: realclean
	mkdir audium 
	mkdir audium/obj
	cp Makefile $(sources) *.h audium 
	tar -cvf - audium | gzip -c > audium.tar.gz 
	rm -rf audium
	

archive	: semiclean
	cd ..; tar -cvf - audium | gzip -c > audium.tar.gz
	
